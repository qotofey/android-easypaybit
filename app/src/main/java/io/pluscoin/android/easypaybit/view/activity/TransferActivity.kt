package io.pluscoin.android.easypaybit.view.activity

import android.content.Intent
import android.support.v4.app.Fragment
import android.util.Log
import com.google.zxing.integration.android.IntentIntegrator
import io.pluscoin.android.easypaybit.view.fragment.TransferFragment

/**
 * Created by Qotof on 13.12.2017.
 */
class TransferActivity : SingleFragmentActivity() {

    interface Callback {
        fun renderTransferEditText(text: String)
    }

    private var mainFragment = TransferFragment()

    override fun createFragment(): Fragment {
        return mainFragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Log.i("MainActivity", "Cancelled scan")
            } else {
                mainFragment.renderTransferEditText(result.contents)
                Log.i("MainActivity", "Scanned " + result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}