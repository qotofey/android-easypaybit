package io.pluscoin.android.easypaybit.view.fragment.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.v7.app.AlertDialog
import io.pluscoin.android.easypaybit.R

/**
 * Created by Qotof on 30.11.2017.
 */
class ErrorSignIn : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setMessage(R.string.error_sign_in)
                .setPositiveButton(android.R.string.ok, null)
                .create()
    }

}