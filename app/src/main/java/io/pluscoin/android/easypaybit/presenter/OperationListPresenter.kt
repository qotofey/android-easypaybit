package io.pluscoin.android.easypaybit.presenter

import io.pluscoin.android.easypaybit.view.OperationListView

/**
 * Created by Qotofey on 29.01.2018.
 */
class OperationListPresenter(private val view: OperationListView) {

    fun initialize() {
        view.hideScreen()
        view.showLoading()
        this.loadOperationList()
    }

    private fun loadOperationList() {

    }

}