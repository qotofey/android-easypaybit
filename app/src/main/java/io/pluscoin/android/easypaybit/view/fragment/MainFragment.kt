package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.*
import android.widget.TextView
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.presenter.MainPresenter
import io.pluscoin.android.easypaybit.view.MainView
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.ImageButton
import io.pluscoin.android.easypaybit.core.OperationBody
import io.pluscoin.android.easypaybit.view.activity.CheckCreationActivity
import io.pluscoin.android.easypaybit.view.activity.OperationActivity
import io.pluscoin.android.easypaybit.view.activity.TransferActivity
import io.pluscoin.android.easypaybit.view.adapter.OperationsAdapter



/**
 * Created by Qotof on 15.11.2017.
 */
class MainFragment : Fragment(), MainView {
    init {
        retainInstance = true
    }

//    interface OperationListListener {
//        fun onOperationClicked(operation: OperationBody)
//    }
//    private val operationListListener: OperationListListener

    private val DIALOG_MAKE_CHECK = "DIALOG_MAKE_CHECK"

    private lateinit var presenter: MainPresenter

    private lateinit var cryptoCurrencyTextView: TextView
    private lateinit var balanceTextView: TextView
    private lateinit var messageVoidHistoryTextView: TextView

    private lateinit var makeCheckButton: Button
    private lateinit var transferButton: Button

    private lateinit var tabLayout: TabLayout

    private lateinit var operationRecyclerView: RecyclerView

    private lateinit var adapter: OperationsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = MainPresenter(this)
        val currencyTarget = Preferences.get(context, Preferences.CURRENCY_TARGET)
        if (currencyTarget.isEmpty()) {
            Preferences.set(context, Preferences.CURRENCY_TARGET, "btc")
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_main, container, false)
        cryptoCurrencyTextView = view.findViewById(R.id.crypto_currency)
        cryptoCurrencyTextView.text = Preferences.get(context, Preferences.CURRENCY_TARGET).toUpperCase()
        balanceTextView = view.findViewById(R.id.number)

        makeCheckButton = view.findViewById(R.id.make_check_button)
        transferButton = view.findViewById(R.id.transfer_button)
        makeCheckButton.setOnClickListener { presenter.showDialogMakeCheck() }
        transferButton.setOnClickListener{ presenter.showDialogTransfer() }

        messageVoidHistoryTextView = view.findViewById(R.id.void_history)

        tabLayout = activity.findViewById(R.id.tabs)
        var positionTabSelected = when(Preferences.get(context, Preferences.CURRENCY_TARGET).toLowerCase()) {
            "btc" -> 0
            "eth" -> 1
            "ltc" -> 2
            "plc" -> 3
            else -> 0
        }
        tabLayout.getTabAt(positionTabSelected)!!.select()

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                Preferences.set(context, Preferences.CURRENCY_TARGET, tab!!.contentDescription.toString())
                presenter.initialize()
            }
        })

        operationRecyclerView = view.findViewById(R.id.operation_list_recycler_view)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.initialize()
    }

    override fun onResume() {
        super.onResume()
        presenter.resume()
    }

    private fun setupRecyclerView() {
        adapter.setOnItemClickListener(onItemClickListener)
        operationRecyclerView.layoutManager = LinearLayoutManager(context)
        operationRecyclerView.adapter = adapter
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showMessageVoidHistory() {
        messageVoidHistoryTextView.visibility = TextView.VISIBLE
    }

    override fun hideMessageVoidHistory() {
        messageVoidHistoryTextView.visibility = TextView.GONE
    }

    override fun getAppContext(): Context {
        return context
    }

    override fun openCheckCreationScreen(currency: String) {
        val activity = getAppContext() as AppCompatActivity
        val intent = Intent(activity, CheckCreationActivity::class.java)
        activity.startActivity(intent)
    }

    override fun openTransferScreen(currency: String) {
        val activity = getAppContext() as AppCompatActivity
        val intent = Intent(activity, TransferActivity::class.java)
        activity.startActivity(intent)
    }

    override fun openOperationScreen(operation: OperationBody) {
        activity.startActivity(OperationActivity.newIntent(context, operation.id))
    }

    override fun renderOperationList(operations: List<OperationBody>) {
        adapter = OperationsAdapter(getAppContext())
        adapter.setOperationList(operations)
        setupRecyclerView()
    }

    override fun renderBalance(balance: String) {
        balanceTextView.text = balance
    }

    override fun renderCurrency(currency: String) {
        cryptoCurrencyTextView.text = currency.toUpperCase()
    }

    private val onItemClickListener = object : OperationsAdapter.OnItemClickListener {
        override fun onOperationItemClicked(operation: OperationBody) {
            presenter.onOperationClicked(operation)
        }
    }
}