package io.pluscoin.android.easypaybit.presenter

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import io.pluscoin.android.easypaybit.core.RegistrationBody
import io.pluscoin.android.easypaybit.core.UserRegistrationBody
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.RegistrationView
import io.pluscoin.android.easypaybit.view.fragment.AuthorizationFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_fragment.*

/**
 * Created by Qotof on 16.11.2017.
 */
class RegistrationPresenter(private val view: RegistrationView) {

    fun initialize() {
        view.hideLoading()
    }

    fun tryRegister(email: String, firstPassword: String, secondPassword: String) {
        var approved = true
        if (email.isEmpty()) {
            approved = false
            view.showErrorEmptyEmail()
        }
        if (firstPassword.isEmpty()) {
            approved = false
            view.showErrorEmptyPassword()
        }
        if (secondPassword.isEmpty()) {
            approved = false
            view.showErrorEmptySecondPassword()
        }
        if (!firstPassword.equals(secondPassword)) {
            approved = false
            view.showErrorEquivalentPassword()
        }
        if (approved) {
            registration(email, firstPassword, secondPassword)
        }
    }

    fun openAuthorizationScreen() {
        val activity = view.getAppContext() as AppCompatActivity
        activity.finish()
    }

    private fun registration(email: String, firstPassword: String, secondPassword: String) {
        view.showLoading()
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).postSignUpData(
                RegistrationBody(
                        UserRegistrationBody(email, firstPassword, secondPassword)
                )
        )
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.hideLoading()
                            openAuthorizationScreen()
                        },
                        { error ->
                            view.hideLoading()
                            view.showDataError()
                        }
                )
    }


}