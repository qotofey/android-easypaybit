package io.pluscoin.android.easypaybit.view.fragment.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.dialog.DialogMakeCheckPresenter
import io.pluscoin.android.easypaybit.view.fragment.MainFragment

/**
 * Created by Qotof on 11.12.2017.
 */
class DialogMakeCheck : DialogFragment() {

    private lateinit var presenterDialog: DialogMakeCheckPresenter
    private lateinit var currencyTarget: String

    private lateinit var numberEditText: EditText

    fun setCurrencyTarget(currency: String) {
        currencyTarget = currency
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenterDialog = DialogMakeCheckPresenter(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_make_check, null)
        val layoutNumberEditText = view.findViewById<TextInputLayout>(R.id.layout_number_currency_field)
        numberEditText = view.findViewById(R.id.number_currency_field)

        return AlertDialog.Builder(activity)
                .setView(view)
                .setTitle(R.string.create_check)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, {
                    dialogInterface, i ->
                    if (numberEditText.text.toString() != "" && numberEditText.text.toString().toDouble() != 0.0) {
                        presenterDialog.createCheck(currencyTarget, numberEditText.text.toString().toDouble())
                    }
                })
                .create()
    }



}