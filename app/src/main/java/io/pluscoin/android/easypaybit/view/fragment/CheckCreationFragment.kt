package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.CheckCreationPresenter
import io.pluscoin.android.easypaybit.view.CheckCreationView

/**
 * Created by Qotof on 13.12.2017.
 */
class CheckCreationFragment : Fragment(), CheckCreationView {

    private lateinit var presenter: CheckCreationPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var layoutAmountCurrency: TextInputLayout
    private lateinit var amountCurrencyEditText: EditText
    private lateinit var createCheckButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = CheckCreationPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_check_creation, container, false)
        progressBar = view.findViewById(R.id.progress_bar)
        layoutAmountCurrency = view.findViewById(R.id.layout_amount_currency)
        
//        layoutAmountCurrency.setOnKeyListener { view, i, keyEvent -> layoutAmountCurrency.isErrorEnabled }
        amountCurrencyEditText = view.findViewById(R.id.amount_currency)

        createCheckButton = view.findViewById(R.id.create_check_button)
        createCheckButton.setOnClickListener {
            val amount = amountCurrencyEditText.text.toString()
            if (amount.isEmpty()) {
                layoutAmountCurrency.error = getString(R.string.error_edit_text_void)
            } else if (amount.toDouble() <= 0) {
                layoutAmountCurrency.error = getString(R.string.error_amount_not_null)
            } else {
                presenter.onClickCreateCheck(amount)
            }
        }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun renderHintCurrency(currency: String) {
        layoutAmountCurrency.hint = currency.toUpperCase()
    }

    override fun onClickCreateCheck() {
        try {
            activity.finish()
        } catch (e: NullPointerException) {}
    }

    override fun showLoading() {
        progressBar.visibility = ProgressBar.VISIBLE
        amountCurrencyEditText.isEnabled = false
        createCheckButton.isEnabled = false
    }

    override fun hideLoading() {
        progressBar.visibility = ProgressBar.GONE
        amountCurrencyEditText.isEnabled = true
        createCheckButton.isEnabled = true
    }

    override fun getAppContext(): Context {
        return context
    }
}