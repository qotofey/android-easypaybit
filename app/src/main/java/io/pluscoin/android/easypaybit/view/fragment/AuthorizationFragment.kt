package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.model.EmailList
import io.pluscoin.android.easypaybit.presenter.AuthorizationPresenter
import io.pluscoin.android.easypaybit.view.AuthorizationView
import io.pluscoin.android.easypaybit.view.fragment.dialog.ErrorSignIn

/**
 * Created by Qotof on 15.11.2017.
 */
class AuthorizationFragment : Fragment(), AuthorizationView {

    private val DIALOG_ERROR_SIGN_IN = "DIALOG_ERROR_SIGN_IN"

    private lateinit var presenter: AuthorizationPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var linearLayout: LinearLayout
    private lateinit var loginLayout: TextInputLayout
    private lateinit var passwordLayout: TextInputLayout

    private lateinit var loginTextField: AutoCompleteTextView
    private lateinit var passwordTextField: EditText
    private lateinit var authorizationButton: Button
    private lateinit var registrationButton: Button
    private lateinit var recoveryButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = AuthorizationPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_authorization, container, false)
//        val collapsingToolbarLayout = activity.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
//        collapsingToolbarLayout.isNestedScrollingEnabled = false

        linearLayout = view.findViewById(R.id.linear_layout)
        progressBar = view.findViewById(R.id.progress_bar)

        loginLayout = view.findViewById(R.id.layout_login_field)
        passwordLayout = view.findViewById(R.id.layout_password_field)

        loginTextField = view.findViewById(R.id.login_text_field)
        loginTextField.setAdapter(
                ArrayAdapter(
                        activity,
                        android.R.layout.simple_dropdown_item_1line,
                        EmailList(context).getEmailList()
                )
        )
        passwordTextField = view.findViewById(R.id.first_password_text_field)

        authorizationButton = view.findViewById(R.id.authorization_button)
        registrationButton = view.findViewById(R.id.registration_button)
        recoveryButton = view.findViewById(R.id.recovery_button)

        authorizationButton.setOnClickListener { this.authorize() }
        registrationButton.setOnClickListener { this.openRegistrationScreen() }
        recoveryButton.setOnClickListener { this.openRecoveryScreen() }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun showLoading() {
        progressBar.visibility = ProgressBar.VISIBLE
        linearLayout.visibility = LinearLayout.INVISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = ProgressBar.GONE
        linearLayout.visibility = LinearLayout.VISIBLE
    }

    override fun showLoginError() {
        loginLayout.error = getString(R.string.error_edit_text_void)
    }

    override fun showPasswordError() {
        passwordLayout.error = getString(R.string.error_edit_text_void)
    }

    override fun hideLoginError() {
        loginLayout.isErrorEnabled = false
    }

    override fun hidePasswordError() {
        passwordLayout.isErrorEnabled = false
    }

    override fun showErrorData() {
        val manager = activity.fragmentManager
        val dialog = ErrorSignIn()
        dialog.show(manager, DIALOG_ERROR_SIGN_IN)
    }

    override fun openRecoveryScreen() {
        presenter.openRecoveryScreen()
    }

    override fun openRegistrationScreen() {
        presenter.openRegistrationScreen()
    }

    override fun authorize() {
        presenter.tryLogIn(
                loginTextField.text.toString(),
                passwordTextField.text.toString()
        )
    }

    override fun getAppContext(): Context {
        return context
    }
}