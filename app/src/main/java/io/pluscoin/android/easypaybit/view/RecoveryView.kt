package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotof on 16.11.2017.
 */
interface RecoveryView : LoadingView {

    fun sendQueryRecovery()

    fun openAuthorizationScreen()
    fun openRegistrationScreen()

}