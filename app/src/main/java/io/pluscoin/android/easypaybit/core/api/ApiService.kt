package io.pluscoin.android.easypaybit.core.api

import io.pluscoin.android.easypaybit.core.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Qotof on 29.11.2017.
 */
interface ApiService {

    @POST("api/v1/public/mobsessions/sign_in")
    fun postSignInData(@Body body: UserBody): Observable<UserResponse>

    @POST("api/v1/public/mobsessions/sign_up")
    fun postSignUpData(@Body body: RegistrationBody): Observable<UserResponse>

    @POST("api/v1/public/mobsessions/forgot")
    fun postRecoveryData(@Body body: RecoveryBody): Observable<TextBody>

    /**
     * @param currency - название валюты, по которой требуется история операций (btc, eth, ltc)
     * @param page - номер страницы
     * @param per - количество элементов на странице
     */
    @GET("api/v1/public/mobuser/operations")
    fun getOperationsData(
            @Query("currency") currency: String,
            @Query("page") page: Int,
            @Query("per") per: Int): Observable<OperationArrayBody>

    @GET("api/v1/public/mobuser/operation")
    fun getOperationData(@Query("id") id: Long): Observable<OperationSingle>

    @GET("api/v1/public/mobuser/balance")
    fun getBalanceData(@Query("currency") currency: String): Observable<Balance>

    @POST("api/v1/public/actions/create_check")
    fun postCreatedCheckData(@Body body: ResultCheck): Observable<ResultCreatedCheck>

    @POST("api/v1/public/actions/create_withdrawal")
    fun postTransferData(@Body body: TransferBody): Observable<ResultCreatedCheck>

}