package io.pluscoin.android.easypaybit.presenter

import android.util.Log
import io.pluscoin.android.easypaybit.core.ResultCheck
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.CheckCreationView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 13.12.2017.
 */
class CheckCreationPresenter(private val view: CheckCreationView) {

    private val currency = Preferences.get(view.getAppContext(), Preferences.CURRENCY_TARGET)

    fun initialize() {
        view.hideLoading()
        view.renderHintCurrency(currency)
    }

    fun onClickCreateCheck(amount: String) {
        view.showLoading()
        val createdCheckBody = ResultCheck(currency, amount.toDouble())
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).postCreatedCheckData(createdCheckBody)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.onClickCreateCheck()
                        },
                        { error ->
                            Log.e("ERROR", "error")
                            view.onClickCreateCheck()
                        }
                )

    }



}