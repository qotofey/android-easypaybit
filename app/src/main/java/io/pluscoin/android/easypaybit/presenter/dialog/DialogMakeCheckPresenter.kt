package io.pluscoin.android.easypaybit.presenter.dialog

import android.util.Log
import io.pluscoin.android.easypaybit.core.ResultCheck
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.fragment.dialog.DialogMakeCheck
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 11.12.2017.
 */
class DialogMakeCheckPresenter(private val view: DialogMakeCheck) {

//    private lateinit var currency: String

//    fun initialize(currency: String) {
//        this.currency = currency
//    }

    fun createCheck(currency: String, amount: Double) {
        val createdCheckBody = ResultCheck(currency, amount)
        val observable = RetrofitHelper.createRetrofit(view.dialog.context).postCreatedCheckData(createdCheckBody)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            Log.i("Dialog", "result = " + result.status)
                        },
                        { error ->
                            Log.e("Dialog", "error")
                        }
                )

    }

}