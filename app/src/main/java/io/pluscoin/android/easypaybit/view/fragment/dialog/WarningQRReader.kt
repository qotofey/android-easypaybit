package io.pluscoin.android.easypaybit.view.fragment.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.v7.app.AlertDialog
import io.pluscoin.android.easypaybit.R

/**
 * Created by Qotofey on 25.01.2018.
 */
class WarningQRReader : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setMessage(R.string.warning_reader_qr_code)
                .setPositiveButton(android.R.string.ok, null)
                .create()
    }

}