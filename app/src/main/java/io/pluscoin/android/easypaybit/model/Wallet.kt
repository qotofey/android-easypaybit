package io.pluscoin.android.easypaybit.model

/**
 * Created by Qotof on 01.12.2017.
 */
class Wallet(val type: String) {

    var address: String = ""
    var count: Double = 0.0

}