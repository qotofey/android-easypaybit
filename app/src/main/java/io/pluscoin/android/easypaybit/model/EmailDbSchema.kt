package io.pluscoin.android.easypaybit.model

/**
 * Created by Qotof on 30.11.2017.
 */
class EmailDbSchema {

    object EmailTable {

        val NAME = "emails"

        object Cols {
            val EMAIL = "email"
        }

    }

}