package io.pluscoin.android.easypaybit.core
/**
 * Created by Qotof on 29.11.2017.
 */

data class UserBody(
		val user: UserModel
)

data class UserModel(
		val email: String, //alex-agildin@mail.ru
		val password: String //123123
)

data class UserResponse(
        val token: String,
        val user_id: Long
)

//для get-запроса получения списка операций
data class CurrencyBody(
		val currency: String, //btc
 		val page: Int, //0
		val per: Int //4
)

data class OperationArrayBody(
		val operations: List<OperationBody>,
		val page: Int, //0
		val per: Int, //4
		val total: Int //7
)

data class OperationSingle(
		val operation: OperationBody
)

data class OperationBody(
		val id: Long, //13
		val updated_at: Double, //1512497477.911766
		val created_at: Double, //1512497477.911766
		val amount: Double, //3.0e-05
		val txn_id: String, //CPBL5BJPWZOXJJ4KJRX1RUBP5S
		val incoming: Boolean, //true
		val status: Int, //0
		val currency: String, //eth
		val address: String //0xc8a19b21ff45713182405ccafedd48cffaaadcec
)

//получаем баланс
data class CurrencyBalance(
		val currency: String //btc
)

data class Balance(
		val ac_bal: Double, //0.3
		val wd_bal: Double, //0.1
		val currency: String //btc
)
//создаём чек
data class ResultCheck(
		val currency: String, //eth
		val amount: Double //0.0004
)

data class ResultCreatedCheck(
		val status: String, //success
		val operation_id: Int //7
)

data class TransferBody(
		val currency: String, //eth
		val amount: Double, //0.0004
		val address: String //.....
)

data class RegistrationBody(
		val user: UserRegistrationBody
)

data class UserRegistrationBody(
		val email: String, //alex-agildin1@mail.ru
		val password: String, //123123
		val password_confirmation: String //123123
)

data class RecoveryBody(
		val user: UserRecoveryBody
)

data class UserRecoveryBody(
		val email: String //alex-agildin1@mail.ru
)

data class TextBody(
		val text: String //Instruction for reset password was sended to email: alex-agildin2@mail.ru
)

