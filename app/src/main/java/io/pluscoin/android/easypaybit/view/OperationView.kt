package io.pluscoin.android.easypaybit.view

import io.pluscoin.android.easypaybit.core.OperationBody
/**
 * Created by Qotof on 14.12.2017.
 */
interface OperationView : LoadingView {

    fun renderOperation(operation: OperationBody)

    fun showScreen()
    fun hideScreen()

}