package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotof on 15.11.2017.
 */
interface AuthorizationView : LoadingView {

    fun openRegistrationScreen()
    fun openRecoveryScreen()

    fun authorize()

    fun showErrorData()
    fun showLoginError()
    fun showPasswordError()

    fun hideLoginError()
    fun hidePasswordError()

}