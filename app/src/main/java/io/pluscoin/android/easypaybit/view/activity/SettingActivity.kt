package io.pluscoin.android.easypaybit.view.activity

import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.SettingFragment

/**
 * Created by Qotofey on 27.01.2018.
 */
class SettingActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return SettingFragment()
    }
}