package io.pluscoin.android.easypaybit.presenter

import io.pluscoin.android.easypaybit.view.SettingView

/**
 * Created by Qotofey on 27.01.2018.
 */
class SettingPresenter(private val view: SettingView) {

    fun initialize() {
        view.hideScreen()
        view.showLoading()
        this.loadCurrencyList()
    }

    private fun loadCurrencyList() {

    }

}