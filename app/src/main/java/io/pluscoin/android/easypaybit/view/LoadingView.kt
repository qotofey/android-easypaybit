package io.pluscoin.android.easypaybit.view

import android.content.Context

/**
 * Created by Qotof on 15.11.2017.
 */
interface LoadingView {

    fun showLoading()

    fun hideLoading()

    fun getAppContext() : Context

}