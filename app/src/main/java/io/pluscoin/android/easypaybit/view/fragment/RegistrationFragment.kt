package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.RegistrationPresenter
import io.pluscoin.android.easypaybit.view.RegistrationView
import io.pluscoin.android.easypaybit.view.fragment.dialog.ErrorEquivalentPasswordDialog
import io.pluscoin.android.easypaybit.view.fragment.dialog.ErrorSignUp


/**
 * Created by Qotof on 15.11.2017.
 */
class RegistrationFragment : Fragment(), RegistrationView {

    private val DIALOG_ERROR_EQUIVALENT = "DIALOG_ERROR_EQUIVALENT"
    private val DIALOG_ERROR_SIGN_UP= "DIALOG_ERROR_SIGN_UP"

    private lateinit var presenter: RegistrationPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var registrationButton: Button
    private lateinit var emailLayout: TextInputLayout
    private lateinit var firstPasswordLayout: TextInputLayout
    private lateinit var secondPasswordLayout: TextInputLayout

    private lateinit var emailField: AutoCompleteTextView
    private lateinit var firstPasswordField: EditText
    private lateinit var secondPasswordField: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = RegistrationPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_registration, container, false)
        progressBar = view.findViewById(R.id.progress_bar)

        emailField = view.findViewById(R.id.email_text_field)
        emailLayout = view.findViewById(R.id.layout_email_field)
        firstPasswordField = view.findViewById(R.id.first_password_text_field)
        firstPasswordLayout = view.findViewById(R.id.layout_first_password_field)
        secondPasswordField = view.findViewById(R.id.second_password_text_field)
        secondPasswordLayout = view.findViewById(R.id.layout_second_password_field)

        registrationButton = view.findViewById(R.id.registration_button)
        registrationButton.setOnClickListener { this.register() }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun showLoading() {
        emailField.isEnabled = false
        firstPasswordField.isEnabled = false
        secondPasswordField.isEnabled = false
        registrationButton.isEnabled = false
        progressBar.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        emailField.isEnabled = true
        firstPasswordField.isEnabled = true
        secondPasswordField.isEnabled = true
        registrationButton.isEnabled = true
        progressBar.visibility = ProgressBar.GONE
    }

    override fun showDataError() {
        val manager = activity.fragmentManager
        val dialog = ErrorSignUp()
        dialog.show(manager, DIALOG_ERROR_SIGN_UP)
    }

    override fun register() {
        presenter.tryRegister(
                emailField.text.toString(),
                firstPasswordField.text.toString(),
                secondPasswordField.text.toString()
        )
    }

    override fun openAuthorizationScreen() {
        //не требуется
    }

    override fun openRecoveryScreen() {
        //не требуется
    }

    override fun showErrorEmptyName() {
        //не требуется
    }

    override fun showErrorEmptyEmail() {
        emailLayout.error = getString(R.string.error_edit_text_void)
    }

    override fun showErrorEmptyPassword() {
        firstPasswordLayout.error = getString(R.string.error_edit_text_void)
    }

    override fun showErrorEmptySecondPassword() {
        secondPasswordLayout.error = getString(R.string.error_edit_text_void)
    }

    override fun showErrorEquivalentPassword() {
        val manager = activity.fragmentManager
        val dialog = ErrorEquivalentPasswordDialog()
        dialog.show(manager, DIALOG_ERROR_EQUIVALENT)
    }

    override fun getAppContext(): Context {
        return context
    }
}

