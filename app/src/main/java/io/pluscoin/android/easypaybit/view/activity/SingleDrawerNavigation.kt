package io.pluscoin.android.easypaybit.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity

import android.view.MenuItem
import android.widget.TextView
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.core.util.Preferences
import kotlinx.android.synthetic.main.activity_fragment.*

/**
 * Created by Qotof on 30.11.2017.
 */
abstract class SingleDrawerNavigation : SingleFragmentActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val header = navigationView.getHeaderView(0)
        val nameTextView = header.findViewById<TextView>(R.id.name_label_drawer_navigation_header)
        val emailTextView = header.findViewById<TextView>(R.id.email_label_drawer_navigation_header)
        nameTextView.text = Preferences.get(this, Preferences.USERNAME)
        emailTextView.text = Preferences.get(this, Preferences.EMAIL)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.nav_wallets) {
//            Snackbar.make(fragment_container, "КОШЕЛЬКИ", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
        } else if (id == R.id.nav_history) {

            val intent = Intent(this, OperationListActivity::class.java)
            this.startActivity(intent)

        } else if (id == R.id.nav_settings) {

            val intent = Intent(this, SettingActivity::class.java)
            this.startActivity(intent)

        } else if (id == R.id.nav_logout) {
            Preferences.clean(this)
            val intent = Intent(this, AuthorizationActivity::class.java)
            startActivity(intent)
            finish()
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

}