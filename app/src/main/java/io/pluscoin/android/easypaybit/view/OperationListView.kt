package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotofey on 29.01.2018.
 */
interface OperationListView : LoadingView {

    fun hideScreen()
    fun showScreen()

}