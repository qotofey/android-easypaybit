package io.pluscoin.android.easypaybit.model

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import io.pluscoin.android.easypaybit.model.EmailDbSchema.EmailTable.Cols



/**
 * Created by Qotof on 30.11.2017.
 */
class EmailList(private val context: Context) {

    private val database: SQLiteDatabase = EmailBaseHelper(context).writableDatabase

    fun add(email: String) {
        if (!getEmailList().contains(email)) {
            Log.e("НЕТ", "Похожего элемента нет!")
            val values = getContentValues(email)
            database.insert(EmailDbSchema.EmailTable.NAME, null, values)
        } else {
            Log.e("ДА", "Похожий элемент есть!")
        }
    }

    fun getEmailList(): List<String> {
        val emails = ArrayList<String>()
        val cursor = queryEmails(null, null)
        try {
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                emails.add(cursor.getString(cursor.getColumnIndex(EmailDbSchema.EmailTable.Cols.EMAIL)))
                cursor.moveToNext()
            }
        } finally {
            cursor.close()
        }
        return emails
    }

    private fun queryEmails(whereClause: String?, whereArgs: Array<String>?): Cursor {
        val cursor = database.query(
                EmailDbSchema.EmailTable.NAME,
                null, // columns - с null выбираются все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        )
        return cursor
    }

    private fun getContentValues(email: String): ContentValues {
        val values = ContentValues()
        values.put(EmailDbSchema.EmailTable.Cols.EMAIL, email)
        return values
    }

}