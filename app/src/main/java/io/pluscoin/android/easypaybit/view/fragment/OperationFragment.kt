package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.core.OperationBody
import io.pluscoin.android.easypaybit.presenter.OperationPresenter
import io.pluscoin.android.easypaybit.view.OperationView
import com.google.zxing.WriterException
import android.graphics.Bitmap
import android.support.constraint.ConstraintLayout
import android.widget.ProgressBar
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.MultiFormatWriter
import java.text.SimpleDateFormat

/**
 * Created by Qotof on 14.12.2017.
 */
class OperationFragment : Fragment(), OperationView {

    private lateinit var presenter: OperationPresenter

    companion object {
        private val ARG_OPERATION_ID = "OPERATION_ID"

        fun newInstance(id: Long): OperationFragment {
            val args = Bundle()
            args.putSerializable(ARG_OPERATION_ID, id)
            val fragment = OperationFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var screen: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var amountCurrencyTextView: TextView
    private lateinit var statusOperationTextView: TextView
    private lateinit var incomingTextView: TextView
    private lateinit var datetimeTextView: TextView
    private lateinit var qrCodeImage: ImageView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        presenter = OperationPresenter(this)
        presenter = OperationPresenter(this, arguments.getLong(ARG_OPERATION_ID))
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_operation, container, false)
        amountCurrencyTextView = view.findViewById(R.id.amount_currency)
        statusOperationTextView = view.findViewById(R.id.status_operation)
        incomingTextView = view.findViewById(R.id.incoming)
        datetimeTextView = view.findViewById(R.id.datetime)
        qrCodeImage = view.findViewById(R.id.qr_code_image_view)
        screen = view.findViewById(R.id.screen)
        progressBar = view.findViewById(R.id.progress_bar)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun renderOperation(operation: OperationBody) {
        amountCurrencyTextView.text = "${operation.amount} ${operation.currency.toUpperCase()}"
        statusOperationTextView.text = when(operation.status) {
            0 -> getString(R.string.pending)
            1 -> getString(R.string.confirm)
            2 -> getString(R.string.canceled)
            else -> ""
        }
        incomingTextView.text = if (operation.incoming) "ваш чек" else "не ваш чек"
        val unixTime = Math.round(operation.created_at * 1000)
        val thatTime = SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(unixTime)
        datetimeTextView.text = thatTime
        ////////////////////
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode("${operation.address};${operation.amount}", BarcodeFormat.QR_CODE, qrCodeImage.width, qrCodeImage.width)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            qrCodeImage.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showScreen() {
        screen.visibility = View.VISIBLE
    }

    override fun hideScreen() {
        screen.visibility = View.INVISIBLE
    }

    override fun getAppContext(): Context {
        return context
    }
}