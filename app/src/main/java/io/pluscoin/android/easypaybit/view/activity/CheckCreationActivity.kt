package io.pluscoin.android.easypaybit.view.activity

import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.CheckCreationFragment

/**
 * Created by Qotof on 13.12.2017.
 */
class CheckCreationActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return CheckCreationFragment()
    }
}