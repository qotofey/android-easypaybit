package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotof on 13.12.2017.
 */
interface CheckCreationView : LoadingView {

    fun onClickCreateCheck()
    fun renderHintCurrency(currency: String)

}