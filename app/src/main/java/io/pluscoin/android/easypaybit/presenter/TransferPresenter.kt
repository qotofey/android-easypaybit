package io.pluscoin.android.easypaybit.presenter

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.R.id.fragment_container
import io.pluscoin.android.easypaybit.R.id.screen
import io.pluscoin.android.easypaybit.core.TransferBody
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.TransferView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 13.12.2017.
 */
class TransferPresenter(private val view: TransferView) {

    private val REQUEST_PHOTO = 1

    private val currency = Preferences.get(view.getAppContext(), Preferences.CURRENCY_TARGET)

    fun initialize() {
        view.hideLoading()
    }

    fun transferMoney(amount: Double, address: String) {
        view.showLoading()
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).postTransferData(TransferBody(currency, amount, address))
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.hideLoading()
                            (view.getAppContext() as AppCompatActivity).finish()
                        },
                        { error ->
                            view.hideLoading()
                            view.showErrorAmountTransfer()
                        }
                )
    }

    fun readQrCode() {
//        val captureImage = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val integrator = IntentIntegrator(view.getAppContext() as Activity)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();

    }

    fun activityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Log.e("MainActivity", "Cancelled scan")
                Toast.makeText(view.getAppContext(), "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                Log.e("MainActivity", "Scanned " + result.contents)
                Toast.makeText(view.getAppContext(), "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
            }
        }
    }



}