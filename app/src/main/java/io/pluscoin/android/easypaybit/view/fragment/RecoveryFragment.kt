package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.RecoveryPresenter
import io.pluscoin.android.easypaybit.view.RecoveryView

/**
 * Created by Qotof on 16.11.2017.
 */
class RecoveryFragment : Fragment(), RecoveryView {

    private lateinit var presenter: RecoveryPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var emailField: AutoCompleteTextView
    private lateinit var recoveryButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = RecoveryPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_recovery, container, false)
        progressBar = view.findViewById(R.id.progress_bar)
        emailField = view.findViewById(R.id.login_text_field)
        recoveryButton = view.findViewById(R.id.recovery_button)
        recoveryButton.setOnClickListener { this.sendQueryRecovery() }
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun sendQueryRecovery() {
        presenter.recovery(emailField.text.toString())
    }

    override fun getAppContext(): Context {
        return context
    }

    override fun openAuthorizationScreen() {
        //не требуется
    }

    override fun openRegistrationScreen() {
        //не требуется
    }

    override fun showLoading() {
        emailField.isEnabled = false
        recoveryButton.isEnabled = false
        progressBar.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        emailField.isEnabled = true
        recoveryButton.isEnabled = true
        progressBar.visibility = ProgressBar.GONE
    }

}