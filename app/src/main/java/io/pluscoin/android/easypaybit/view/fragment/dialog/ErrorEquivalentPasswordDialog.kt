package io.pluscoin.android.easypaybit.view.fragment.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.v7.app.AlertDialog
import io.pluscoin.android.easypaybit.R

/**
 * Created by Qotof on 23.11.2017.
 */
class ErrorEquivalentPasswordDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setMessage(R.string.error_equivalent_password)
                .setPositiveButton(android.R.string.ok, null)
                .create()
    }

}