package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotof on 15.11.2017.
 */
interface RegistrationView : LoadingView {

    fun openAuthorizationScreen()
    fun openRecoveryScreen()

    fun register()

    fun showErrorEmptyName()
    fun showErrorEmptyEmail()
    fun showErrorEmptyPassword()
    fun showErrorEmptySecondPassword()
    fun showErrorEquivalentPassword()

    fun showDataError()

}