package io.pluscoin.android.easypaybit.view.activity

import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.OperationListFragment

/**
 * Created by Qotofey on 29.01.2018.
 */
class OperationListActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return OperationListFragment()
    }
}