package io.pluscoin.android.easypaybit.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Qotof on 30.11.2017.
 */
open class EmailBaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, VERSION) {

    companion object {
        private val VERSION = 1
        private val DATABASE_NAME = "emailBase.db"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
                "create table ${EmailDbSchema.EmailTable.NAME}" +
                "( _id integer primary key autoincrement, ${EmailDbSchema.EmailTable.Cols.EMAIL})"
        )
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}