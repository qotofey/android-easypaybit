package io.pluscoin.android.easypaybit.view.activity

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.OperationFragment

/**
 * Created by Qotof on 14.12.2017.
 */
class OperationActivity : SingleFragmentActivity() {

    companion object {

        private val INTENT_EXTRA_PARAM_OPERATION_ID = "io.pluscoin.android.easypaybit.OPERATION_ID"

        fun newIntent(context: Context, id: Long): Intent {
            val intent = Intent(context, OperationActivity::class.java)
            intent.putExtra(INTENT_EXTRA_PARAM_OPERATION_ID, id)
            return intent
        }

    }

    override fun createFragment(): Fragment {
        val operationId = intent.getLongExtra(INTENT_EXTRA_PARAM_OPERATION_ID, -1)
        return OperationFragment.newInstance(operationId.toString().toLong())
    }
}