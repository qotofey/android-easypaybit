package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotof on 13.12.2017.
 */
interface TransferView : LoadingView {

    fun transferMoney()

    fun readQRCode()

    fun showErrorReader()
    fun showErrorAmountTransfer()


}