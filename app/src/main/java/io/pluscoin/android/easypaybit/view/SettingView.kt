package io.pluscoin.android.easypaybit.view

/**
 * Created by Qotofey on 27.01.2018.
 */
interface SettingView : LoadingView {

    fun renderCurrencyList()
    fun showScreen()
    fun hideScreen()

}