package io.pluscoin.android.easypaybit.presenter

import io.pluscoin.android.easypaybit.core.RecoveryBody
import io.pluscoin.android.easypaybit.core.UserRecoveryBody
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.RecoveryView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 16.11.2017.
 */
class RecoveryPresenter(private val view: RecoveryView) {

    fun initialize() {
        view.hideLoading()
    }

    fun recovery(login: String) {
        view.showLoading()
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).postRecoveryData(RecoveryBody(UserRecoveryBody(login)))
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.hideLoading()
//                            this.openMainScreen()
                        },
                        { error ->
                            view.hideLoading()

                        }
                )

    }

}