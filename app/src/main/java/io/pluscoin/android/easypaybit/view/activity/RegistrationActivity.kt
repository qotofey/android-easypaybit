package io.pluscoin.android.easypaybit.view.activity

import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.RegistrationFragment

/**
 * Created by Qotof on 15.11.2017.
 */
class RegistrationActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return RegistrationFragment()
    }
}