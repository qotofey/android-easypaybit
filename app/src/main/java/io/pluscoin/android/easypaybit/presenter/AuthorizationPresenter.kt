package io.pluscoin.android.easypaybit.presenter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.pluscoin.android.easypaybit.core.UserBody
import io.pluscoin.android.easypaybit.core.UserModel
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.model.EmailList
import io.pluscoin.android.easypaybit.view.AuthorizationView
import io.pluscoin.android.easypaybit.view.activity.MainActivity
import io.pluscoin.android.easypaybit.view.activity.RecoveryActivity
import io.pluscoin.android.easypaybit.view.activity.RegistrationActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 15.11.2017.
 */
class AuthorizationPresenter(
        private val view: AuthorizationView) {

    companion object val TAG = "AuthorizationPresenter"

    fun initialize() {
        view.showLoading()
        try {
            this.authorize(
                    Preferences.get(view.getAppContext(), Preferences.EMAIL),
                    Preferences.get(view.getAppContext(), Preferences.PASSWORD)
            )
        } catch (e: KotlinNullPointerException) {
            view.hideLoading()
        }
    }

    fun tryLogIn(login: String, password: String) {
        view.showLoading()
        if (login.isEmpty()) {
            view.showLoginError()
        } else {
            view.hideLoginError()
        }
        if (password.isEmpty()) {
            view.showPasswordError()
        } else {
            view.hidePasswordError()
        }
        if (!login.isEmpty() && !password.isEmpty()) {
            this.authorize(login, password)
        } else {
            view.hideLoading()
        }
    }

    private fun openMainScreen() {
        val activity = view.getAppContext() as AppCompatActivity
        val intent = Intent(activity, MainActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun openRegistrationScreen() {
        val activity = view.getAppContext() as AppCompatActivity
        val intent = Intent(activity, RegistrationActivity::class.java)
        activity.startActivity(intent)
//        activity.finish()
    }

    fun openRecoveryScreen() {
        val activity = view.getAppContext() as AppCompatActivity
        val intent = Intent(activity, RecoveryActivity::class.java)
        activity.startActivity(intent)
//        activity.finish()
    }

    private fun authorize(login: String, password: String) {
        val userBody = UserBody(UserModel(login, password))
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).postSignInData(userBody)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            //сохраняем данные для автоматического входа: токен, почту, пароль
                            Preferences.set(view.getAppContext(), Preferences.TOKEN, result.token)
                            Preferences.set(view.getAppContext(), Preferences.EMAIL, userBody.user.email)
                            Preferences.set(view.getAppContext(), Preferences.PASSWORD, userBody.user.password)
                            Log.e(TAG, result.token)
                            Log.e(TAG, result.user_id.toString())
                            //сохраняем кошельки
//                            Preferences.set(view.getAppContext(), Preferences.ETH_WALLET, result.wallet)
                            //добавляем новый email в БД для удобсва
                            EmailList(view.getAppContext()).add(userBody.user.email)
                            this.openMainScreen()
                        },
                        { error ->
                            view.hideLoading()
                            Log.e(TAG, error.message.toString())
                            if (!login.isEmpty() && !password.isEmpty()) {
                                view.showErrorData()
                            }
                        }
                )

    }

}