package io.pluscoin.android.easypaybit.core.util

import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import io.pluscoin.android.easypaybit.view.activity.AuthorizationActivity
import io.pluscoin.android.easypaybit.view.activity.RecoveryActivity
import io.pluscoin.android.easypaybit.view.fragment.AuthorizationFragment

/**
 * Created by Qotof on 29.11.2017.
 */
object Preferences {

    val TOKEN = "io.pluscoin.android.easypaybit.core.util.Preferences.TOKEN"
    val EMAIL = "io.pluscoin.android.easypaybit.core.util.Preferences.EMAIL"
    val PASSWORD = "io.pluscoin.android.easypaybit.core.util.Preferences.PASSWORD"
    val USERNAME = "io.pluscoin.android.easypaybit.core.util.Preferences.USERNAME"

    val USER_ID = "io.pluscoin.android.easypaybit.core.util.Preferences.USER_ID"

    val BTC_WALLET = "io.pluscoin.android.easypaybit.core.util.Preferences.BTC_WALLET"
    val ETH_WALLET = "io.pluscoin.android.easypaybit.core.util.Preferences.ETH_WALLET"
    val LTC_WALLET = "io.pluscoin.android.easypaybit.core.util.Preferences.LTC_WALLET"

    val CURRENCY_TARGET = "io.pluscoin.android.easypaybit.core.util.Preferences.CURRENCY_TARGET"

    //ДОБАВИЛ КОНСТАНТУ? НЕ ЗАБУДЬ ПОМЕНЯТЬ МЕТОД clean!!!

    fun set(context: Context, key: String, value: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply()
    }

    fun set(context: Context, key: String, value: Long) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(key, value).apply()
    }

    fun set(context: Context, key: String, value: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply()
    }

    fun get(context: Context, key: String): String {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, "")
    }

    fun getLong(context: Context, key: String): Long {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(key, 0L)
    }

    fun getBoolean(context: Context, key: String): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false)
    }

    fun clean(context: Context) {
        Preferences.set(context, Preferences.TOKEN, "")             //string
        Preferences.set(context, Preferences.EMAIL, "")             //string
        Preferences.set(context, Preferences.PASSWORD, "")          //string
        Preferences.set(context, Preferences.USERNAME, "")          //string
        Preferences.set(context, Preferences.USER_ID, -1L)          //long
        Preferences.set(context, Preferences.BTC_WALLET, "")        //string
        Preferences.set(context, Preferences.ETH_WALLET, "")        //string
        Preferences.set(context, Preferences.LTC_WALLET, "")        //string
        Preferences.set(context, Preferences.CURRENCY_TARGET, "")   //string
    }

}