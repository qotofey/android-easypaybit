package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.TransferPresenter
import io.pluscoin.android.easypaybit.view.TransferView
import android.widget.*
import com.google.zxing.integration.android.IntentIntegrator
import io.pluscoin.android.easypaybit.view.activity.QRCodeReaderActivity
import io.pluscoin.android.easypaybit.view.activity.TransferActivity
import io.pluscoin.android.easypaybit.view.fragment.dialog.WarningQRReader
import io.reactivex.exceptions.OnErrorNotImplementedException
import retrofit2.adapter.rxjava2.HttpException


/**
 * Created by Qotof on 13.12.2017.
 */
class TransferFragment : Fragment(), TransferView, TransferActivity.Callback {

    private val DIALOG_WARNING_QR_CODR_READER = "DIALOG_WARNING_QR_CODR_READER"

    override fun renderTransferEditText(text: String) {
        if (!text.contains(';')) {
            addressWalletEditText.setText(text)
            amountCurrencyEditText.setText("")
        } else {
            try {
                addressWalletEditText.setText(text.substring(0, text.indexOf(';')))
                amountCurrencyEditText.setText(text.substring(text.indexOf(';') + 1, text.length))
            } catch (e: UninitializedPropertyAccessException) {
                Log.e("TransferFragment", "throw UninitializedPropertyAccessException")
//                this.showErrorReader()
            }
        }
    }

    private lateinit var presenter: TransferPresenter

    private lateinit var selfView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = TransferPresenter(this)
    }

    private lateinit var screenView: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var addressWalletEditText: TextInputEditText
    private lateinit var amountCurrencyEditText: EditText
    private lateinit var transferButton: Button
    private lateinit var readerQRButton: ImageButton

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_transfer, container, false)
        screenView = view.findViewById(R.id.screen)
        progressBar = view.findViewById(R.id.progress_bar)
        addressWalletEditText = view.findViewById(R.id.address_wallet)
        amountCurrencyEditText = view.findViewById(R.id.amount_currency)
        transferButton = view.findViewById(R.id.transfer_button)
        transferButton.setOnClickListener { this.transferMoney() }
        readerQRButton = view.findViewById(R.id.qr_reader_button)
        readerQRButton.setOnClickListener{
            this.readQRCode()
        }
        selfView = view
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun transferMoney() {
        if (amountCurrencyEditText.text.isEmpty() || amountCurrencyEditText.text.toString().toDouble() == 0.0) {
            amountCurrencyEditText.error = "void"
        } else if (addressWalletEditText.text.isEmpty()) {
            addressWalletEditText.error = "void"
        } else {
            try {
                presenter.transferMoney(
                        amountCurrencyEditText.text.toString().toDouble(),
                        addressWalletEditText.text.toString()
                )
            } catch (e: NullPointerException) {

            } catch (e: OnErrorNotImplementedException) {

            } catch (e: HttpException) {

            }
        }
    }

    override fun showErrorAmountTransfer() {
        Snackbar.make(selfView, R.string.warning_amount_transfer, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    override fun readQRCode() {
        val integrator = IntentIntegrator(activity)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
        integrator.setPrompt(getString(R.string.title_activity_qr_code_scanner))
        integrator.setCameraId(0)
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(false)
        integrator.setOrientationLocked(false)
        integrator.setCaptureActivity(QRCodeReaderActivity::class.java)
        integrator.initiateScan()
    }

    override fun showErrorReader() {
        val manager = activity.fragmentManager
        val dialog = WarningQRReader()
        dialog.show(manager, DIALOG_WARNING_QR_CODR_READER)
    }

    override fun showLoading() {
        addressWalletEditText.isEnabled = false
        amountCurrencyEditText.isEnabled = false
        transferButton.isEnabled = false
        readerQRButton.isEnabled = false
        progressBar.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        addressWalletEditText.isEnabled = true
        amountCurrencyEditText.isEnabled = true
        transferButton.isEnabled = true
        readerQRButton.isEnabled = true
        progressBar.visibility = ProgressBar.GONE
    }

    override fun getAppContext(): Context {
        return context
    }
}