package io.pluscoin.android.easypaybit.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.core.OperationBody
import io.pluscoin.android.easypaybit.core.util.Preferences

/**
 * Created by Qotof on 12.12.2017.
 */
class OperationsAdapter(private val context: Context) : RecyclerView.Adapter<OperationsAdapter.OperationHolder>() {

    interface OnItemClickListener {
        fun onOperationItemClicked(operation: OperationBody)
    }
    private lateinit var callback: OnItemClickListener
    fun setOnItemClickListener(callback: OnItemClickListener) {
        this.callback = callback
    }

    private lateinit var operationArray: List<OperationBody>

    override fun getItemCount(): Int {
        return operationArray.size
    }


    fun setOperationList(operations: List<OperationBody>) {
        operationArray = operations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OperationHolder {
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.list_item_operation, parent, false)
        return OperationHolder(view)
    }

    override fun onBindViewHolder(holder: OperationHolder?, position: Int) {
        holder!!.bindOperation(context, operationArray[position])
        holder.itemView.setOnClickListener {
            callback.onOperationItemClicked(operationArray[position])
        }
    }

    class OperationHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        private val statusImageView: ImageView = itemView!!.findViewById(R.id.image_status)
        private val statusTextView: TextView = itemView!!.findViewById(R.id.status_opearion)
        private val amountCurrencyView: TextView = itemView!!.findViewById(R.id.amount_currency)

        fun bindOperation(context: Context, operation: OperationBody) {
//            statusImageView =
            statusTextView.text = when(operation.status) {
                0 -> context.getString(R.string.pending)
                1 -> context.getString(R.string.confirm)
                2 -> context.getString(R.string.canceled)
                else -> "Error"
            }
            amountCurrencyView.text =
                    "${operation.amount} ${Preferences.get(context, Preferences.CURRENCY_TARGET).toUpperCase()}"
        }
    }

}