package io.pluscoin.android.easypaybit.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.view.fragment.AuthorizationFragment

/**
 * Created by Qotof on 15.11.2017.
 */
class AuthorizationActivity : SingleFragmentActivity() {

    override fun getLayoutResId(): Int {
        return R.layout.toolbar_scrolling
    }

    override fun createFragment(): Fragment {
        return AuthorizationFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }
}