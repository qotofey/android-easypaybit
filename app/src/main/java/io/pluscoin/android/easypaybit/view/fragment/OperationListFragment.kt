package io.pluscoin.android.easypaybit.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import io.pluscoin.android.easypaybit.R
import io.pluscoin.android.easypaybit.presenter.OperationListPresenter
import io.pluscoin.android.easypaybit.view.OperationListView

/**
 * Created by Qotofey on 29.01.2018.
 */
class OperationListFragment : Fragment(), OperationListView {
    init {
        retainInstance = true
    }

    private lateinit var presenter: OperationListPresenter

    private lateinit var screen: ConstraintLayout
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = OperationListPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_operation_list, container, false)
        screen = view.findViewById(R.id.screen)
        progressBar = view.findViewById(R.id.progressBar)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initialize()
    }

    override fun showScreen() {
        screen.visibility = View.VISIBLE
    }

    override fun hideScreen() {
        screen.visibility = View.GONE
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun getAppContext(): Context {
        return context
    }
}