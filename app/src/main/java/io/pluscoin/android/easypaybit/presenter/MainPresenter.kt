package io.pluscoin.android.easypaybit.presenter

import android.util.Log
import io.pluscoin.android.easypaybit.core.OperationBody
import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.MainView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 16.11.2017.
 */
class MainPresenter(private val view: MainView) {

//    private lateinit var operationArray: OperationArrayBody

    private lateinit var currencyTarget: String

    fun initialize() {
        currencyTarget = Preferences.get(view.getAppContext(), Preferences.CURRENCY_TARGET)
        Preferences.set(view.getAppContext(), Preferences.CURRENCY_TARGET, currencyTarget.toLowerCase())
        this.loadWallet()
        this.loadOperationList()
    }

    fun showDialogMakeCheck() {
        view.openCheckCreationScreen(currencyTarget)
    }

    fun showDialogTransfer() {
        view.openTransferScreen(currencyTarget)
    }

    fun resume() {
        this.loadWallet()
        this.loadOperationList()
    }

    fun onOperationClicked(operation: OperationBody) {
        view.openOperationScreen(operation)
    }

    private fun loadWallet() {
        this.getWallet()
    }

    private fun loadOperationList() {
        this.hideMessageVoidHistory()
        this.showViewLoading()
        this.getOperationList()
    }

    private fun showViewLoading() {
        view.showLoading()
    }

    private fun hideViewLoading() {
        view.hideLoading()
    }

    private fun showMessageVoidHistory() {
        view.showMessageVoidHistory()
    }

    private fun hideMessageVoidHistory() {
        view.hideMessageVoidHistory()
    }

    fun getCurrencyTarget(): String {
        return currencyTarget
    }

    private fun getWallet() {
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).getBalanceData(currencyTarget)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.renderBalance(result.ac_bal.toString())
                            view.renderCurrency(result.currency)
                        }
                )
    }

    private fun getOperationList() {
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).getOperationsData(
                currencyTarget.toLowerCase(),
                0,
                20
        )
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            Log.e("TAG, страница: ", result.page.toString())
                            Log.e("TAG, записей: ", result.per.toString())
                            view.renderOperationList(result.operations)
                            if (result.operations.size > 0) {
                                this.hideMessageVoidHistory()
                            } else {
                                this.showMessageVoidHistory()
                            }
                        }
                )
    }

}