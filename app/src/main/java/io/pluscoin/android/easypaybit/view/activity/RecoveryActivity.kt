package io.pluscoin.android.easypaybit.view.activity

import android.support.v4.app.Fragment
import io.pluscoin.android.easypaybit.view.fragment.RecoveryFragment

/**
 * Created by Qotof on 16.11.2017.
 */
class RecoveryActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return RecoveryFragment()
    }
}