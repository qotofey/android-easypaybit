package io.pluscoin.android.easypaybit.presenter

import io.pluscoin.android.easypaybit.core.api.RetrofitHelper
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.OperationView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Qotof on 14.12.2017.
 */
class OperationPresenter(
        private val view: OperationView,
        private val operationId: Long) {

    fun initialize() {
        view.showLoading()
        view.hideScreen()
        this.loadOperation()
    }

    private fun loadOperation() {
        this.getOperation()
    }

    private fun getOperation() {
        val observable = RetrofitHelper.createRetrofit(view.getAppContext()).getOperationData(operationId)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view.renderOperation(result.operation)
                            view.hideLoading()
                            view.showScreen()
                        }
                )
    }



}