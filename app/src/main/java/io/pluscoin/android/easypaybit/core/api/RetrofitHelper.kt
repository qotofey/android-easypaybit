package io.pluscoin.android.easypaybit.core.api

import android.content.Context
import android.content.Intent
import android.util.Log
import io.pluscoin.android.easypaybit.core.util.Preferences
import io.pluscoin.android.easypaybit.view.activity.AuthorizationActivity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Qotof on 29.11.2017.
 */
object RetrofitHelper {

    private fun createOkHttpClient(context: Context): OkHttpClient {
        val httpClient = OkHttpClient.Builder()

        httpClient.interceptors().add(Interceptor { chain ->
            val originalRequest = chain.request()
            val originalHttpUrl = originalRequest.url()

            val url = originalHttpUrl
                    .newBuilder()
                    .build()

            val requestBuilder = originalRequest
                    .newBuilder()
                    .url(url)
                    .header("Authorization", Preferences.get(context, Preferences.TOKEN))
                    .header("Content-Type", "application/json")
                    .method(originalRequest.method(), originalRequest.body())

            val request = requestBuilder.build()

//            chain.proceed(request)
            val response = chain.proceed(request)
//            Log.e("TAG", response.code().toString())
//            Log.e("TAG", response.message())
//            Log.e("TAG", response.networkResponse().toString())
//            Log.e("TAG", response.body()!!.source().readUtf8())
//            when (response.code()) {
//                400 -> {
////                    Preferences.clean(context)
////                    context.startActivity(Intent(context, AuthorizationActivity::class.java))
//                }
//                401 -> {
//                    Preferences.clean(context)
//                    context.startActivity(Intent(context, AuthorizationActivity::class.java))
//                }
//                422 -> {
////                    Preferences.clean(context)
////                    context.startActivity(Intent(context, AuthorizationActivity::class.java))
//                }
//                429 -> {
////                    Preferences.clean(context)
////                    context.startActivity(Intent(context, AuthorizationActivity::class.java))
//                }
//
//            }

            response //возвращается
        })

        return httpClient.build()
    }

    fun createRetrofit(context: Context): ApiService {
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://easypay.dsplus.pro/")
                .client(createOkHttpClient(context))
                .build()

        return retrofit.create(ApiService::class.java)
    }

}