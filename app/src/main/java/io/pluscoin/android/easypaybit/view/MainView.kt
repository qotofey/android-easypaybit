package io.pluscoin.android.easypaybit.view

import io.pluscoin.android.easypaybit.core.OperationBody

/**
 * Created by Qotof on 15.11.2017.
 */
interface MainView : LoadingView {

    fun openCheckCreationScreen(currency: String)
    fun openTransferScreen(currency: String)
    fun openOperationScreen(operation: OperationBody)


    fun renderOperationList(operations: List<OperationBody>)
    fun renderBalance(balance: String)
    fun renderCurrency(currency: String)

    fun showMessageVoidHistory()
    fun hideMessageVoidHistory()

}